<?php
	require '../vendor/autoload.php';
	include_once("../config.inc.php");
	include_once("AppTestClass.php");
	

	$user = "demo";
	$password = "demopass";
	$project = "demo";

	$Args = Array(
			"user" => Array(
				"name" => $user,
				"password" => $password,
				"domain" => Array(
					"name" => "Default")
				),
			"scope" => Array(
				"project" => Array(
					"name" => $project,
					"domain" => Array(
						"name" => "Default")
					)
				),
			"authUrl" => $config["urlAuth"]
		);
	
	$App = new AppTest($Args);

?>
