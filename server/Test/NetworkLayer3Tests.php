<?php

include('InitTest.php');
include_once("../core/Compute.php");
include_once("../core/Network.php");
include_once("../core/NetworkLayer3.php");

$compute = new Compute($App);
$network = new Network($App);
$networkLayer3 = new NetworkLayer3($App);


// Liste des serveurs
echo "Liste des serveurs : </br>";
$compute->action("listServers");
$servers = json_decode($App->show(), true)["Servers"];
$id = null;
foreach($servers as $server){
	echo $server['name']." ".$server['id']." ".$server['ipv4']."<br>";
	if(strcmp($server['name'], "bob")){
		$id = $server['id'];
	}	
}
echo "</br>";


// Liste des networks
echo "Liste des network : </br>";
$network->action("list_network_ids");
$servers = json_decode($App->show(), true)["ListNetworkIds"];
$id = null;
foreach($servers as $server){
	echo $server."<br>";
}
echo "</br>";


// Liste des floatingip
echo "Liste des floatingip : </br>";
$networkLayer3->action("listFloatingIp");
$listFloatingIp =  json_decode($App->show(), true)["NetworkLayer3"];
$id = null;
foreach ($listFloatingIp as $floatIp){
	echo $floatIp['floatingIpAddress']." ".$floatIp['id']." ".$floatIp["status"]."<br>";
	$id = $floatIp['id'];
}
echo "</br>";


// get floatingip
echo "Get floatingip : </br>";
$App->setPostParam('id', $id);
$networkLayer3->action("getFloatingIp");
$getFloatingIp =  json_decode($App->show(), true)["NetworkLayer3"];
echo $getFloatingIp['id']."<br>";
echo "</br>";


/*
// Création d'une ip flotante
$opt = array();
$opt['floatingNetworkId'] = "251b4641-20ff-4a72-8549-1758788b51ce";

$App->setPostParam('opt', $opt);
$networkLayer3->action("createFloatingIp");
$float =  json_decode($App->show(), true)["NetworkLayer3"];
if(!isset($float)){
	echo "Erreur pendant la création</br>";
}
echo "</br>";
*/


/*
// Suppression d'une ip flotante
$App->setPostParam('id', $id);
$networkLayer3->action("deleteFloatingIp");
*/


// Liste des floatingip
echo "Liste des floatingip : </br>";
$networkLayer3->action("listFloatingIp");
$listFloatingIp =  json_decode($App->show(), true)["NetworkLayer3"];
foreach ($listFloatingIp as $floatIp){
	echo $floatIp['floatingIpAddress']." ".$floatIp['id']." ".$floatIp["status"]."<br>";
}
echo "</br>";


// Liste des routeurs
echo "Liste des routeurs : </br>";
$networkLayer3->action("listRouters");
$listRouters =  json_decode($App->show(), true)["NetworkLayer3"];
foreach ($listRouters as $router){
	echo $router['name']." ".$router['id']."<br>";
	if(strcmp($router['name'], "Test")){
		$id = $router['id'];
	}
}
echo "</br>";


// get floatingip
echo "Get router : </br>";
$App->setPostParam('id', $id);
$networkLayer3->action("getRouter");
$getRouter =  json_decode($App->show(), true)["NetworkLayer3"];
echo $getRouter['id']."<br>";
echo "</br>";


/*
// Création d'un routeur'		
$opt = array();
$optGate = array();
$optGate['networkId'] = "251b4641-20ff-4a72-8549-1758788b51ce";
$opt['externalGatewayInfo'] = $optGate;
$opt['name'] = "Test";
$App->setPostParam('opt', $opt);
$networkLayer3->action("createRouter");
$r =  json_decode($App->show(), true)["NetworkLayer3"];
if(!isset($r)){
	echo "Erreur pendant la création</br>";
}
echo "</br>";
*/


/*
// Suppression d'un routeur
$App->setPostParam('id', $id);
$networkLayer3->action("deleteRouter");
echo "</br>";
*/


// Liste des routeurs
echo "Liste des routeurs : </br>";
$networkLayer3->action("listRouters");
$listRouters =  json_decode($App->show(), true)["NetworkLayer3"];
foreach ($listRouters as $router){
	echo $router['name']." ".$router['id']."<br>";
}

?>