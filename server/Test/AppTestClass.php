<?php
include_once("../core/LibOverride/genTokenOptions.php");
include_once("../core/ErrorManagement.php");

use OpenCloud\Common\Error\BadResponseError;
use OpenCloud\Common\Error\BaseError;
use OpenCloud\Common\Error\NotImplementedError;
use OpenCloud\Common\Error\UserInputError;

class AppTest{
	
	protected $openstack;
	protected $postParams;
	protected $tokenClass;
	protected $tokenPost;
	protected $output;
	protected $errorClass;

	public function __construct($args){

		$this->tokenPost = NULL;
		$this->tokenClass = new genTokenOptions($args);
		$this->openstack = new OpenStack\OpenStack(['authUrl' => $args["authUrl"]]);
		$this->errorClass = new errorManagement($this);
		$this->output = array();

		$this->errorClass = new errorManagement($this);

		$this->postParams = $_POST;

	}
	
	public function setToken($token){
		
		$this->tokenPost = $token;
		$this->tokenClass->loadBackup($his->tokenPost);
		
	}
	
	public function getLibClass($service){
		
		switch($service){
			case "Identity":
				if($this->tokenPost == NULL) $this->tokenClass->genIdentityToken();
				$opt = $this->tokenClass->getOptions($service);
				return $this->openstack->identityV3($opt);
				break;
			case "Image":
				if($this->tokenPost == NULL) $this->tokenClass->genImageToken();
				$opt = $this->tokenClass->getOptions($service);
				return $this->openstack->imagesV2($opt);
				break;
			case "Compute":
				if($this->tokenPost == NULL) $this->tokenClass->genComputeToken();
				$opt = $this->tokenClass->getOptions($service);
				return $this->openstack->computeV2($opt);
				break;
			case "Network":
				if($this->tokenPost == NULL) $this->tokenClass->genNetworkToken();
				$opt = $this->tokenClass->getOptions($service);
				return $this->openstack->networkingV2($opt);
				break;
			case "NetworkLayer3":
				if($this->tokenPost == NULL) $this->tokenClass->genNetworkToken();
				$opt = $this->tokenClass->getOptions('Network');
				return $this->openstack->networkingV2ExtLayer3($opt);
				break;
		}
		
	}
	
	public function authenticate(){
		
		try{
			$this->tokenClass->genIdentityToken();
			$this->tokenClass->genComputeToken();
			$this->tokenClass->genImageToken();
			$this->tokenClass->genNetworkToken();
			
			$this->setOutput("token", $this->tokenClass->getBackup());
		}catch(BadResponseError $e){
			$this->errorClass->BadResponseHandler($e);
        }catch(UserInputError $e){
			$this->errorClass->UserInputHandler($e);
        }catch(BaseError $e){
			$this->errorClass->BaseErrorHandler($e);
        }catch(NotImplementedError $e){
			$this->errorClass->NotImplementedHandler($e);
        }
		
	}
	
	public function getPostParam($name){
		if(isset($this->postParams[$name])){
			return $this->postParams[$name];
		}else{
			$this->setOutput("Error", "Missing parameter ".$name);
		}
	}

	public function setPostParam($name, $value){
		
		$this->postParams[$name] = $value;

	}
	
	public function setOutput($key, $out){
		
		$this->output[$key] = $out;
		
	}
	
	public function show(){
		return json_encode($this->output);
	}
	

	public function getErrorInstance(){
		
		return $this->errorClass;
		
	}
}


