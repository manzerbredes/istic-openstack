<?php
	include_once("config.inc.php");
	require "../vendor/autoload.php"; 
	include_once("core/Plugin_Api.php");
	include_once("core/LibOverride/genTokenOptions.php");
	
	$user = "admin";
		$password = "ae5or6cn";
		$project = "admin";
		
		$Args = Array(
			"user" => Array(
				"name" => $user,
				"password" => $password,
				"domain" => Array(
					"name" => "Default")
				),
			"scope" => Array(
				"project" => Array(
					"name" => $project,
					"domain" => Array(
						"name" => "Default")
					)
				),
			"authUrl" => $config["urlAuth"]
		);

	$genOptions = new genTokenOptions($Args);
	$genOptions->genIdentityToken();
	$genOptions->genComputeToken();
	$genOptions->genNetworkToken();
	$genOptions->genImageToken();
	
	$backCompute = $genOptions->getBackup("Compute");
	$backIdentity = $genOptions->getBackup("Identity");
	$backNetwork = $genOptions->getBackup("Network");
	$backImage = $genOptions->getBackup("Image");
	
	$openstack_api = new OpenStack\OpenStack([]);

	$newGenOptions = new genTokenOptions($Args);
	$newGenOptions->loadIdentityBackup($backIdentity);
	$newGenOptions->loadComputeBackup($backCompute);
	$newGenOptions->loadImageBackup($backImage);
	$newGenOptions->loadNetworkBackup($backNetwork);
	
	$optionsCompute = $newGenOptions->getOptions("Compute");
	$optionsIdentity = $newGenOptions->getOptions("Identity");
	$optionsNetwork = $newGenOptions->getOptions("Network");
	$optionsImage = $newGenOptions->getOptions("Image");

	$identityTest = $openstack_api->identityV3($optionsIdentity);
	$computeTest = $openstack_api->computeV2($optionsCompute);
	$networkTest = $openstack_api->networkingV2($optionsNetwork);
	$imageTest = $openstack_api->imagesV2($optionsImage);
	
	$domainsTest = $identityTest->listDomains();
	echo "Identity Test, List Domains </br>";
	foreach($domainsTest as $domain){
		echo $domain->id;
		echo "</br>";
	}
	echo "</br>";
	
	$imagesTest = $imageTest->listImages();
	echo "Image Test, List Images </br>";
	foreach($imagesTest as $image){
		echo $image->id;
		echo "</br>";
	}
	echo "</br>";
	$serversTest = $computeTest->listServers();
	echo "Compute Test, List Servers </br>";
	foreach($serversTest as $server){
		echo $server->id;
		echo "</br>";
	}
	echo "</br>";
	$networkTest = $networkTest->listNetworks();
	echo "Network Test, List networks </br>";
	foreach($networkTest as $network){
		echo $network->id;
		echo "</br>";
	}
	
?>