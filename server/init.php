<?php
/**
 * File containing the initialisation of the API.
 *
 * @version 1.0 Initialisation of this file
 * @since 1.0 Core application's file
 * 
 * @author Eole 'eoledev at outlook . fr'
 * 
 */

/*
* Include Main Api Class
*/ 
include_once("core/App.php");

$user = "";
$password = "";
$project = "";


//token processing
if(isset($_POST["token"])){
	
	$token = $_POST["token"];
	
}else if(isset($_POST["user"]) && isset($_POST["password"]) && isset($_POST["project"]) ){	
	
	$user = $_POST["user"];
	$password = $_POST["password"];
	$project = $_POST["project"];
	
}

//Library args
$Args = Array(
	"user" => Array(
		"name" => $user,
		"password" => $password,
		"domain" => Array(
			"name" => "Default")
	),
	"scope" => Array(
		"project" => Array(
			"name" => $project,
			"domain" => Array(
				"name" => "Default")
			)
		),
	"authUrl" => $config["urlAuth"]
);

//Init core Api
$App = new App($Args);

if(isset($token))
	$App->setToken($token);
?>
