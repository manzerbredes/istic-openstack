<?php
/**
* File containing the code for the API door.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author Eole 'eoledev at outlook . fr'
* 
*/

/*
*/
//loading dependencies
require "vendor/autoload.php";
//Include general config file
include_once("config.inc.php");
//Include API initialisation
include_once("init.php");

if(isset($_POST["task"]) && isset($_POST["action"])){
	$task = $_POST["task"];
	$action = $_POST["action"];
}else if(isset($_POST["task"]) && $_POST["task"] == "Authenticate" || $_POST["task"] == "Deauthenticate"){
	$task = $_POST["task"];
}else{
	$App->setOutput("Error", "Invalid Request!");
	$App->show();
	exit();
}

//Authentification and deauthentification request
if($task == "Authenticate"){
	
	$App->authenticate();
	$App->show();
	
}else if($task == "Deauthenticate"){
	
	$App->deauthenticate();
	$App->show();
	
}else if($App->checkToken()){
	//Task switcher and task's file loader
	switch($task)
	{
	case "identity":
		include_once("core/Identity.php");
		$identityObject = new identity($App);
		$identityObject->action($action);
		$App->show();
		break;
		
	case "network":
		include_once("core/Network.php");
		$networkObject = new network($App);
		$networkObject->action($action);
		$App->show();
		break;	
		
	case "image":
		include_once("core/Image.php");
		$imageObject = new image($App);
		$imageObject->action($action);
		$App->show();
		break;

	case "compute":
		include_once("core/Compute.php");
		$computeObject = new compute($App);
		$computeObject->action($action);
		$App->show();
		break;
		
	case "networkLayer3":
		include_once("core/NetworkLayer3.php");
		$computeObject = new networkLayer3($App);
		$computeObject->action($action);
		$App->show();
		break;
	}
	
}else{
	//Request without authentication
	$App->setOutput("Error", "Token Invalide");
	$App->show();
}



