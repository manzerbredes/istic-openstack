<?php
/**
* File containing the networkLayer3 Class.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author Evan Pisani 'yogg at epsina . com'
* 
*/

use OpenCloud\Common\Error\BadResponseError;
use OpenCloud\Common\Error\BaseError;
use OpenCloud\Common\Error\NotImplementedError;
use OpenCloud\Common\Error\UserInputError;

include("CoreInterface.php");

/**
* networkLayer3 Class of the back-end application
*
* Management of networkLayer3
*
*/
class networkLayer3 implements Core{ 

	/** @var App $app protected, contains the main app object */
	protected $app;
	
	/** @var OpenStack\NetworkLayer3 $libClass protected, contains the library NetworkLayer3 object */
	protected $libClass;
	
	/**
	* networkLayer3 constructor
	*
	* @param App $app the main app object
	*
	* @return networkLayer3 Object
	*/
	public function __construct($app){
		if(!isset($app)){
			$this->app->setOutput("Error", "Incorrect parameter app");
		}
		$this->app = $app;
		$this->libClass = $app->getLibClass("NetworkLayer3");
	}


	/**
	* Execute an action
	*
	* @param String $action name of another function of this class
	*
	* @return void
	*/
	public function action($action){
		$this->{$action.""}();	
	}


	/**
	* List floatingip
	* 
	* @return void
	*/
	private function listFloatingIp(){
		try{
			$result = array();
			$l = $this->libClass->listFloatingIps();
			error_log(var_export($l, true), 0);
			foreach ($l as $tmp) {
				$result[] = $tmp;
			}

			$this->app->setOutput("NetworkLayer3", $result);
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}

	/**
	* Create a new floating IP adress
	*
	* @param array $opt Options for the floating ip creation (floatingNetworkId is required)
	*
	* @return void
	*/
	private function createFloatingIp(){
		$opt = $this->app->getPostParam("opt");

		if(!isset($opt)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			$floatingip = $this->libClass->createFloatingIp($opt);

			if(!isset($floatingip)){
				$this->app->setOutput("Error", "Unknowing error during floating ip creation");
			}else{
				$this->app->setOutput("NetworkLayer3", $floatingip);
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* Show floatingip details
	*
	* @param String id the id of the floatingip
	*
	* @return void
	*/
	private function getFloatingIp(){
		$id = $this->app->getPostParam("id");
		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}

		try{
			// List of floating IPs
			$res = array();
			$l = $this->libClass->listFloatingIps();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
					
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknow id");
			}else{ // If id exists
				$res = $this->libClass->getFloatingIp($id);
				$this->app->setOutput("NetworkLayer3", $res);
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}

	/**
	* Update floating ip
	*
	* @param id the id of the floatingip to update
	* 
	* @return void
	*/
	private function updateFloatingIp(){
		$id = $this->app->getPostParam("id");

		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{

			// List of floating IPs
			$res = array();
			$l = $this->libClass->listFloatingIps();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
					
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknowing floatingip id");
			}else{
				$result->update();
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}

	/**
	* Delete a floating ip
	*
	* @param string floatingip_id the floating-ip id to delete
	*
	* @return void
	*/
	private function deleteFloatingIp(){
		$id = $this->app->getPostParam("id");

		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			// List of floating IPs
			$res = array();
			$l = $this->libClass->listFloatingIps();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
					
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknowing floatingip id");
			}else{
				$result->delete(); 
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* Retrieve a floating ip
	*
	* @param string floatingip_id the floating-ip id to retrieve
	*
	* @return void
	*/
	private function retrieveFloatingIp(){
		$id = $this->app->getPostParam("id");

		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			// List of floating IPs
			$res = array();
			$l = $this->libClass->listFloatingIps();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
					
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknowing floatingip id");
			}else{
				$result->retrieve();
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}



	/**
	* Create a new router
	*
	* @param array $opt Options for the new router 
	*					externalGatewayInfo[] required (only the param networkId in the tab)
	*					adminStateUp (optionnal)
	*					name (optionnal)
	*
	* @return void
	*/
	private function createRouter(){
		$opt = $this->app->getPostParam("opt");

		if(!isset($opt)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			$router = $this->libClass->createRouter($opt);

			if(!isset($router)){
				$this->app->setOutput("Error", "Unknowing error during floating ip creation");
			}else{
				$this->app->setOutput("NetworkLayer3", $router);
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* List routers
	* 
	* @return void
	*/
	private function listRouters(){
		try{
			$result = array();
			$l = $this->libClass->listRouters();
			foreach ($l as $tmp) {
				$result[] = $tmp;
			}

			$this->app->setOutput("NetworkLayer3", $result);
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* Show router details
	*
	* @param String id the id of the router
	*
	* @return void
	*/
	private function getRouter(){
		$id = $this->app->getPostParam("id");
		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			// List of routers
			$res = array();
			$l = $this->libClass->listRouters();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknow id");
			}else{ // If id exists
				$res = $this->libClass->getRouter($id);
				$this->app->setOutput("NetworkLayer3", $res);
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* Delete a router
	*
	* @param string router the router to delete
	*
	* @return void
	*/
	private function deleteRouter(){
		$id = $this->app->getPostParam("id");

		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{
			// List of routers
			$res = array();
			$l = $this->libClass->listRouters();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknowing router id");
			}else{
				$result->delete(); 
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}


	/**
	* Update router
	*
	* @param id the id of the floatingip to update
	* 
	* @return void
	*/
	private function updateRouter(){
		$id = $this->app->getPostParam("id");

		if(!isset($id)){
			$this->app->setOutput("Error", "Incorrect parameter opt");
		}
		try{

			// List of floating IPs
			$res = array();
			$l = $this->libClass->listRouters();
			foreach ($l as $tmp) {
				$res[] = $tmp;
			}

			// Verification if id exists
			$result = null;
			foreach ($res as $f) {
				if(strcmp($f->id, $id)){
					$result = $f;
					
				}
			}

			if(!isset($result)){ // If id doesn't exists
				$this->app->setOutput("Error", "Unknowing floatingip id");
			}else{
				$result->update();
			}
		}catch(BadResponseError $e){
			$this->app->getErrorInstance()->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->app->getErrorInstance()->UserInputHandler($e);
		}catch(BaseError $e){
			$this->app->getErrorInstance()->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->app->getErrorInstance()->NotImplementedHandler($e);
		}catch(Exception $e){
			$this->app->getErrorInstance()->OtherException($e);
		}
	}
}

