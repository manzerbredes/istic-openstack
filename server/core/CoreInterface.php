<?php
/**
* File containing Core Interface.
* 
*/

/**
* Interface for the main classes of the API
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author Eole 'eoledev at outlook . fr'
*/
interface Core{
	
	/**
	* Execute an action in the class
	*
	* @param String $action Function to be called
	*
	* @return void
	*/
	public function action($action);
	
}