<?php
/**
* File containing the identity Class.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author Eole 'eoledev at outlook . fr'
* 
*/

/*
* Library token management override
*/
include_once("core/LibOverride/genTokenOptions.php");
include_once("core/ErrorManagement.php");

//Library loading
use OpenCloud\Common\Error\BadResponseError;
use OpenCloud\Common\Error\BaseError;
use OpenCloud\Common\Error\NotImplementedError;
use OpenCloud\Common\Error\UserInputError;

/**
* App Class of the back-end application
*
* This class allow the communication between the front-end application and
* the library which allow to send requests to an Openstack instance.
*
*/
class App{
	
	/** @var OpenStack\OpenStack $openstack protected, contains the main library object */
	protected $openstack;
	/** @var Array $postParams protected, contains the post parameters */
	protected $postParams;
	/** @var genTokenOptions $tokenClass protected, contains the class object for the authentication override of the library */
	protected $tokenClass;
	/** @var String $tokenPost protected, contains the token given in parameter */
	protected $tokenPost;
	/** @var errorManagement $errorClass protected, contains the errorManagement object */
	protected $errorClass;
	/** @var Array $output protected, contains the result for the API call */
	protected $output;

	/**
	* App constructor
	*
	* @param Array $args Args for the OpenStack Library
	*
	* @return App object
	*/
	public function __construct($args){

		$this->tokenPost = NULL;
		$this->tokenClass = new genTokenOptions($args);
		$this->openstack = new OpenStack\OpenStack(['authUrl' => $args["authUrl"]]);
		$this->errorClass = new errorManagement($this);
		$this->output = array();
		$this->postParams = $_POST;

	}
	
	/**
	* Set the class var $tokenPost and load the token
	* into the genTokenOptions Class
	*
	* @param String $token token to be set
	*
	*/
	public function setToken($token){
		
		$this->tokenPost = $token;
		$this->tokenClass->loadBackup($this->tokenPost);
		
	}
	
	/**
	* Check the expiration of the token
	*
	* @return Boolean if the token is not expired
	*/
	public function checkToken(){
		return $this->tokenClass->checkToken();
	}
	
	/**
	* Get the service Class given the name in parameter
	*
	* @param String $service Name of the service 
	*
	* @return Core object
	*/
	public function getLibClass($service){
		
		switch($service){
		case "Identity":
			if($this->tokenPost == NULL) $this->tokenClass->genIdentityToken();
			$opt = $this->tokenClass->getOptions($service);
			return $this->openstack->identityV3($opt);
			break;
		case "Image":
			if($this->tokenPost == NULL) $this->tokenClass->genImageToken();
			$opt = $this->tokenClass->getOptions($service);
			return $this->openstack->imagesV2($opt);
			break;
		case "Network":
			if($this->tokenPost == NULL) $this->tokenClass->genNetworkToken();
			$opt = $this->tokenClass->getOptions($service);
			return $this->openstack->networkingV2($opt);
			break;
		case "Compute":
			if($this->tokenPost == NULL) $this->tokenClass->genComputeToken();
			$opt = $this->tokenClass->getOptions($service);
			return $this->openstack->computeV2($opt);
			break;
		case "NetworkLayer3":
			if($this->tokenPost == NULL) $this->tokenClass->genNetworkToken();
			$opt = $this->tokenClass->getOptions('Network');
			return $this->openstack->networkingV2ExtLayer3($opt);
			break;
		}
		
	}
	
	/**
	* Generate the token for the different services in OpenStack
	*
	* @return void
	*/
	public function authenticate(){
		
		try{
			$this->tokenClass->genIdentityToken();
			$this->tokenClass->genComputeToken();
			$this->tokenClass->genImageToken();
			$this->tokenClass->genNetworkToken();
			
			$this->setOutput("token", $this->tokenClass->getBackup());
		}catch(BadResponseError $e){
			$this->errorClass->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->errorClass->UserInputHandler($e);
		}catch(BaseError $e){
			$this->errorClass->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->errorClass->NotImplementedHandler($e);
		}
		
	}
	
	/**
	* Revoke the openstack services' token
	*
	* @return void
	*/
	public function deauthenticate(){
		
		try{
			
			$this->tokenClass->revokeComputeToken();
			$this->tokenClass->revokeImageToken();
			$this->tokenClass->revokeNetworkToken();
			$this->tokenClass->revokeIdentityToken();
			
			$this->setOutput("deauthenticate", "Ok");
		}catch(BadResponseError $e){
			$this->errorClass->BadResponseHandler($e);
		}catch(UserInputError $e){
			$this->errorClass->UserInputHandler($e);
		}catch(BaseError $e){
			$this->errorClass->BaseErrorHandler($e);
		}catch(NotImplementedError $e){
			$this->errorClass->NotImplementedHandler($e);
		}
		
	}
	
	/**
	* Retrieve a post parameter given its name
	*
	* @param String $name Expected post parameter's name
	*
	* @return Object Post value
	*/
	public function getPostParam($name){

		if(isset($this->postParams[$name])){
			return $this->postParams[$name];
		}else{
			$this->setOutput("Error", "Missing parameter ".$name);
		}	
		
	}

	/**
	* Set a post parameter for automating task
	*
	* @param String $param Name for the Post entry
	* @param Object $value Value for the Post entry
	*
	* @return void
	*/
	public function setPostParam($param, $value){
		
		$this->postParams[$param] = $value;
		
	}
	
	/**
	* Set a new output message
	*
	* @param String $key Array key for the message
	* @param Array $out Message's value
	*
	* @return void
	*/
	public function setOutput($key, $out){
		
		$this->output[$key] = $out;
		
	}
	
	/**
	* Retrieve the errorManagement instance Object
	*
	* @return errorManagement object
	*/
	public function getErrorInstance(){
		
		return $this->errorClass;
		
	}
	
	/**
	* Output the messages to be send to the client
	*
	* @return void
	*/
	public function show(){
		echo json_encode($this->output);
	}
	
}
