<?php
/**
* File containing the Image Class.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author KABIR Othmane
* 
*/
use OpenCloud\Common\Error\BadResponseError;
use OpenCloud\Common\Error\BaseError;
use OpenCloud\Common\Error\NotImplementedError;
use OpenCloud\Common\Error\UserInputError;

require_once("CoreInterface.php");

/**
* Network Class of the back-end application
*
* Management of Networks
*
*/
class network implements Core{ 
	/** @var App $app protected, contains the main app object */	
	protected $app;
	/** @var OpenStack\Network $libClass protected, contains the library Network object */	
	protected $libClass;
	
	
	/**
	* Network constructor
	*
	* @param App $app the main app object
	*
	* @return network Object
	*/
	public function __construct($app){
		$this->app = $app;
		$this->libClass = $app->getLibClass("Network");
		
	}

	/**
	* Execute an action
	*
	* @param String $action name of another function of this class
	*
	* @return void
	*/
	public function action($action){
		
		$this->{$action.""}();
		
	}

	/**
	* Create a new network
	*
	* @param String name A human-readable name for the network. This name might not be unique
	* @param String adminStateUp The administrative state of network. If false (down), the network does not forward packets
	* @param String shared Specifies whether the network resource can be accessed by any tenant
	* @param String tenantId Owner of network. Only admin users can specify a tenant ID other than their own
	* 
	* @return void
	*/
	private function create_network()
	{
		$options = array();
		// check the name if it is null
		$name = $this->app->getPostParam("name");
		$adminStateUp = $this->app->getPostParam("adminStateUp");
		$shared = $this->app->getPostParam("shared");
		$tenantId = $this->app->getPostParam("tenantId");

		if (isset($name))
		{
			$options['name'] = $name; 
		}
		// check the adminStateUp if it is null
		if (isset($adminStateUp))
		{
			$options['adminStateUp'] = $adminStateUp; 
		}
		// check the shared if it is null
		if (isset($shared))
		{
			$options['shared'] = $shared; 
		}
		// check the tenantId if it is null
		if (isset($tenantId))
		{
			$options['tenantId'] =$tenantId; 
		}
		try
		{
			$network = $this->libClass->createNetworks($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}		

		
	}
	
	/**
	* Create a new subnet
	*
	* @param String networkId A Network this subnet is associated with
	* @param String ipVersion IP version (4 or 6)
	* @param String cidr CIDR representing the IP address range for this subnet Exmple X.Y.W.Z/12
	* @param String tenantId Owner of network. Only admin users can specify a tenant ID other than their own
	* @param String name A human-readable name for the subnet. This name might not be unique
	* @param String gatewayIp IP address of the default gateway used by devices on this subnet
	* @param String dnsNameservers DNS nameservers used by hosts in this subnet
	* @param String enableDhcp Specifies whether DHCP is enabled for this subnet
	* @param String hostRoutes Routes that should be used by devices with IP addresses from this subnet (not including the local subnet route)
	* @param BOOLEAN enableDhcp Specifies whether DHCP is enabled for this subnet
	* @param String allocationPools Subranges of the CIDR available for dynamic allocation to ports
	* 	
	* @return void
	*/

	private function create_subnet()
	{	$options = array();
		$networkId = $this->app->getPostParam("networkId");
		$ipVersion = $this->app->getPostParam("ipVersion");
		$cidr = $this->app->getPostParam("cidr");
		$tenantId = $this->app->getPostParam("tenantId");
		$name = $this->app->getPostParam("name");
		$gatewayIp = $this->app->getPostParam("gatewayIp");
		$dnsNameservers = $this->app->getPostParam("dnsNameservers");
		$allocationPools = $this->app->getPostParam("allocationPools");
		$hostRoutes = $this->app->getPostParam("hostRoutes");
		$enableDhcp = $this->app->getPostParam("enableDhcp");
		$tenantId = $this->app->getPostParam("tenantId");

		if (isset($networkId))
		{
			$options['networkId'] = $networkId; 
		}
		if (isset($ipVersion))
		{
			$options['ipVersion'] = $ipVersion; 
		}
		if (isset($cidr))
		{
			$options['cidr'] = $cidr; 
		}
		if (isset($tenantId))
		{
			$options['tenantId'] = $tenantId; 
		}
		if (isset($name))
		{
			$options['name'] = $name; 
		}
		if (isset($gatewayIp))
		{
			$options['gatewayIp'] = $gatewayIp; 
		}
		if (isset($dnsNameservers))
		{
			$options['dnsNameservers'] = $dnsNameservers; 
		}
		if (isset($allocationPools))
		{
			$options['allocationPools'] = $allocationPools; 
		}
		if (isset($hostRoutes))
		{
			$options['hostRoutes'] = $hostRoutes; 
		}
		if (isset($enableDhcp))
		{
			$options['enableDhcp'] = $enableDhcp; 
		}
		if (isset($tenantId))
		{
			$options['tenantId'] = $tenantId; 
		}
		
		try
		{
			$subnet = $this->libClass->createSubnet($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	

		
	}
	
	/**
	* List the ID of the NETWORKS
	*
	* @return void
	*/

	private function list_network_ids()
	{
		try
		{
			$ln = $this->libClass->listNetworks();
			
			$list_ids = array();

			
			foreach($ln as $n)
			{
				
				$list_ids[] =  $n->id;
				

			}
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	

		$this->app->setOutput("ListNetworkIds", $list_ids);
	}

	/**
	* List the name of the NETWORKS
	*
	* @return List of Networks name
	*/

	private function list_network_names()
	{
		try
		{
			$ln = $this->libClass->listNetworks();
			$list_names = array();
			
			foreach($ln as $n)
			{
				$list_names[] =  $n->name;
			}
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	

		$this->app->setOutput("ListNetworkNames", $list_names);
	}
	
	/**
	* List the CIDR of the SUBNETS
	*
	* @return void
	*/
	private function list_cidr()
	{
		try
		{
			$ls = $this->libClass->listSubnets();
			$list_cidr = array();
			foreach ($ls as $subnet)
			{
				
				$list_cidr[] = $subnet->cidr;
			}

			$this->app->setOutput("ListCidr", $list_cidr);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	
	/**
	* retrieve a specific network
	*
	* @param networkId ID of network which we want to get
	*
	* @return void
	*/ 
	private function getNetwork()
	{
		$network="";
		
		try
		{	
			$networkId = $this->app->getPostParam("networkId");
			$newtork = $this->libClass->getNetwork($networkId);
			$network->retrieve();			
		}

		
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	

		$this->app->setOutput("Network", $network);
	}
	
	/**
	* internal function
	*
	* @param String netId ID of network which we want to get
	*
	* @return OpenStack\Network
	*/
	private function getNetworkP($netId)
	{
		$network="";
		
		try
		{	$newtork = $this->libClass->getNetwork($netId);
			$network->retrieve();

			
		}

		
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	

		return $network;
	}

	/**
	* retrieve a specific subnet
	*
	* @param subnetId ID of subnet which we want to get
	*
	* @return void
	*/
	private function getSubnet()
	{
		$sbnet="";
		
		try
		{	
			$subnetId = $this->app->getPostParam("subnetId");
			$subnet = $this->libClass->getSubnet($subnetId);
			$subnet->retrieve();
		}

		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
		$this->app->setOutput("Subnet", subnet);
		
	}
	/**
	* internal function
	*
	* @param String subnetId ID of subnet which we want to get
	*
	* @return OpenStack\Subnet
	*/
	private function getSubnetP($subnetId)
	{
		$subnet="";
		
		try
		{	$subnet = $this->libClass->getSubnet($subnetId);
			$subnet->retrieve();

			
		}

		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
		return $subnet;
		
	}		
	
	/**
	* Update a network given
	*
	* @param String name A human-readable name for the network. This name might not be unique
	* @param String adminStateUp The administrative state of network. If false (down), the network does not forward packets
	* @param String shared Specifies whether the network resource can be accessed by any tenant
	* @param String tenantId Owner of network. Only admin users can specify a tenant ID other than their own
	* 
	*
	* @return void	
	**/	

	private function updateNetwork()
	{	
		$options = array();
		$name = $this->app->getPostParam("name");
		$shared = $this->app->getPostParam("shared");
		$adminStateUp = $this->app->getPostParam("adminStateUp");

		if(isset($name))
		{
			$options['name'] = $name;	
		}
		if(isset($shared))
		{
			$options['shared'] = $shared;	
		}
		if(isset($adminStateUp))
		{
			$options['adminStateUp'] = $adminStateUp;	
		}
		try
		{		
			$networkId = $this->app->getPostParam("networkId");
			$network = getNetworkP($networkId);
			
			$network->update($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	
	/**
	* Update a subnet given
	*
	* @param String networkId A Network this subnet is associated with
	* @param String ipVersion IP version (4 or 6)
	* @param String cidr CIDR representing the IP address range for this subnet Exmple X.Y.W.Z/12
	* @param String tenantId Owner of network. Only admin users can specify a tenant ID other than their own
	* @param String name A human-readable name for the subnet. This name might not be unique
	* @param String gatewayIp IP address of the default gateway used by devices on this subnet
	* @param String dnsNameservers DNS nameservers used by hosts in this subnet
	* @param String enableDhcp Specifies whether DHCP is enabled for this subnet
	* @param String hostRoutes Routes that should be used by devices with IP addresses from this subnet (not including the local subnet route)
	* @param BOOLEAN enableDhcp Specifies whether DHCP is enabled for this subnet
	* @param String allocationPools Subranges of the CIDR available for dynamic allocation to ports
	* 
	*
	* @return void	
	**/	

	private function updateSubnet()
	{	
		$options = array();
		$name = $this->app->getPostParam("name");
		$networkId = $this->app->getPostParam("networkId");
		$ipVersion = $this->app->getPostParam("ipVersion");
		$cidr = $this->app->getPostParam("cidr");
		if(isset($name))
		{
			$options['name'] = $name;	
		}
		if(isset($networkId))
		{
			$options['networkId'] = $networkId;	
		}
		if(isset($ipVersion))
		{
			$options['ipVersion'] = $ipVersion;	
		}
		if(isset($cidr))
		{
			$options['cidr'] = $cidr;	
		}
		try
		{
			$networkId = $this->app->getPostParam("networkId");
			$subnet = getSubnetP($networkId);
			$subnet->update($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	
	/**
	* Delete a network given
	*
	* @param String networkId ID if network which we want to delete
	* 
	* @return void	
	**/	
	private function deleteNetwork()
	{
		try
		{
			$networkId = $this->app->getPostParam("networkId");
			$network = getNetworkP($networkId);
			$network->delete();
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	
	/**
	* Delete a subnet given
	*
	* @param String subnetId ID if network which we want to delete
	* 
	* @return void	
	**/
	private function deleteSubnet()
	{
		try
		{		
			$subnetId = $this->app->getPostParam("subnetId");
			$subnet = getNetworkP($subnetId);
			$subnet->delete();
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}

	/**
	* Create a new port
	*
	* @param String networkId 	Network this port is associated with
	* @param String name 	A human-readable name for the port. This name might not be unique
	* @param String adminStateUp 	The administrative state of port. If false (down), the port does not forward packets
	* @param String macAddress 	MAC address to use on this port
	* @param String fixedIps 	IP addresses for this port
	* @param String deviceId 	Identifies the device (for example, virtual server) using this port
	* @param String deviceOwner 	Identifies the entity (for example, DHCP agent) using this port
	* @param String securityGroups 	Specifies the IDs of any security groups associated with this port
	* @param String tenantId 	Owner of the port. Only admin users can specify a tenant ID other than their own.
	*	
	* @return void
	*/
	
	private function createPort()
	{
		$options = array();
		$networkId = $this->app->getPostParam("networkId");
		$name = $this->app->getPostParam("name");
		$adminStateUp = $this->app->getPostParam("adminStateUp");
		$macAddress = $this->app->getPostParam("macAddress");
		$fixedIps = $this->app->getPostParam("fixedIps");
		$deviceId = $this->app->getPostParam("deviceId");
		$deviceOwner = $this->app->getPostParam("deviceOwner");
		$securityGroups = $this->app->getPostParam("securityGroups");
		$tenantId = $this->app->getPostParam("tenantId");

		if (isset($networkId))
		{
			$options['networkId'] = $networkId; 
		}
		if (isset($name))
		{
			$options['name'] = $name; 
		}
		if (isset($adminStateUp))
		{
			$options['adminStateUp'] = $adminStateUp; 
		}
		if (isset($macAddress))
		{
			$options['macAddress'] = $macAddress; 
		}
		if (isset($fixedIps))
		{
			$options['fixedIps'] = $fixedIps; 
		}
		if (isset($deviceId))
		{
			$options['deviceId'] = $deviceId; 
		}
		if (isset($deviceOwner))
		{
			$options['deviceOwner'] = $deviceOwner; 
		}
		if (isset($securityGroups))
		{
			$options['securityGroups'] = $securityGroups; 
		}
		if (isset($tenantId))
		{
			$options['tenantId'] = $tenantId; 
		}
		try
		{
			$this->libClass->createPort($options);	
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	
	/**
	* List the of ports
	*
	* @return void
	*/

	private function listPorts()
	{
		try
		{		
			$this->app->setOutput("listPorts", $this->libClass->listPorts()); 
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}

	/**
	* retrieve a specific port given
	*
	* @param portId ID of port which we want to get
	*
	* @return void
	*/

	private function getPort()
	{
		try
		{
			$portId = $this->app->getPostParam("portId");
			$port = $this->libClass->getport($portId);
			$this->app->setOutput("Port", $port);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
		
	}

	/**
	* internal function
	*
	* retrieve a specific port given
	*
	* @param portId ID of port which we want to get
	*
	* @return port
	*/

	private function getPortP($portId)
	{
		try
		{
			$port = $this->libClass->getport($portId);
			return $port;
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
		
	}		


	/**
	* Update port given
	*
	* @param String networkId 	Network this port is associated with
	* @param String name 	A human-readable name for the port. This name might not be unique
	* @param String adminStateUp 	The administrative state of port. If false (down), the port does not forward packets
	* @param String macAddress 	MAC address to use on this port
	* @param String fixedIps 	IP addresses for this port
	* @param String deviceId 	Identifies the device (for example, virtual server) using this port
	* @param String deviceOwner 	Identifies the entity (for example, DHCP agent) using this port
	* @param String securityGroups 	Specifies the IDs of any security groups associated with this port
	* @param String tenantId 	Owner of the port. Only admin users can specify a tenant ID other than their own.
	*	
	* @return void
	*/
	private function updatePort()
	{
		$options = array();
		$networkId = $this->app->getPostParam("networkId");
		$name = $this->app->getPostParam("name");
		$adminStateUp = $this->app->getPostParam("adminStateUp");
		$macAddress = $this->app->getPostParam("macAddress");
		$fixedIps = $this->app->getPostParam("fixedIps");
		$deviceId = $this->app->getPostParam("deviceId");
		$deviceOwner = $this->app->getPostParam("deviceOwner");
		$securityGroups = $this->app->getPostParam("securityGroups");
		$tenantId = $this->app->getPostParam("tenantId");

		if (isset($networkId))
		{
			$options['networkId'] = $networkId; 
		}
		if (isset($name))
		{
			$options['name'] = $name; 
		}
		if (isset($adminStateUp))
		{
			$options['adminStateUp'] = $adminStateUp; 
		}
		if (isset($macAddress))
		{
			$options['macAddress'] = $macAddress; 
		}
		if (isset($fixedIps))
		{
			$options['fixedIps'] = $fixedIps; 
		}
		if (isset($deviceId))
		{
			$options['deviceId'] = $deviceId; 
		}
		if (isset($deviceOwner))
		{
			$options['deviceOwner'] = $deviceOwner; 
		}
		if (isset($securityGroups))
		{
			$options['securityGroups'] = $securityGroups; 
		}
		if (isset($tenantId))
		{
			$options['tenantId'] = $tenantId; 
		}
		try
		{
			$port = getPortP($networkId);
			$port->update($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}	
	}
	/**
	* Delete a port given
	*
	* @param String portId 	id of port which we wante to delete
	*
	* @return void
	*/
	private function deletePort()
	{
		
		try
		{
			$portId = $this->app->getPostParam("portId");
			$port = getPortP($portId);
			$port->delete();
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}
	
	/**
	* Create a new security groupe 
	*
	* @param String name 	A human-readable name for the security group. This name might not be unique
	* @param String description 	Description of the security group
	*	
	* @return void
	*/

	private function createSecurityGroup()
	{
		$options = array();
		$name = $this->app->getPostParam("name");
		$description = $this->app->getPostParam("description");

		if (isset($name))
		{
			$options['name'] = $name; 
		}
		if (isset($description))
		{
			$options['description'] = $description; 
		}
		try
		{
			$this->libClass->createSecurityGroup($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
		
	}
	
	/**
	* Create a new security groupe 
	*
	* @param String securityGroupId 	The security group ID to associate with this security group rule.
	* @param String direction 	The direction in which the security group rule is applied. For a compute instance, an ingress security group rule is applied to incoming (ingress) traffic for that instance. An egress rule is applied to traffic leaving the instance.
	* @param String ethertype 	Must be IPv4 or IPv6, and addresses represented in CIDR must match the ingress or egress rules.	
	* @param String portRangeMin 	The minimum port number in the range that is matched by the security group rule. If the protocol is TCP or UDP, this value must be less than or equal to the value of the portRangeMax attribute. If the protocol is ICMP, this value must be an ICMP type
	* @param String portRangeMax 	The maximum port number in the range that is matched by the security group rule. If the protocol is TCP or UDP, this value must be less than or equal to the value of the portRangeMax attribute. If the protocol is ICMP, this value must be an ICMP type.
	* @param String protocol 	The protocol that is matched by the security group rule
	* @param String remoteGroupId 	The remote group ID to be associated with this security group rule. You can specify either remoteGroupId or remoteGroupPrefix
	* @param String remoteIpPrefix 	The remote IP prefix to be associated with this security group rule. You can specify either remoteGroupId or remoteGroupPrefix
	*
	* @return void
	*/
	private function createSecurityGroupRule()
	{
		$options = array();
		$securityGroupId = $this->app->getPostParam("securityGroupId");
		$direction = $this->app->getPostParam("direction");
		$ethertype = $this->app->getPostParam("ethertype");
		$portRangeMin = $this->app->getPostParam("portRangeMin");
		$portRangeMax = $this->app->getPostParam("portRangeMax");
		$protocol = $this->app->getPostParam("protocol");
		$remoteGroupId = $this->app->getPostParam("remoteGroupId");
		$remoteIpPrefix = $this->app->getPostParam("remoteIpPrefix");

		if (isset($securityGroupId))
		{
			$options['securityGroupId'] = $securityGroupId; 
		}
		if (isset($direction))
		{
			$options['direction'] = $direction; 
		}
		if (isset($ethertype))
		{
			$options['ethertype'] = $ethertype; 
		}
		if (isset($portRangeMin))
		{
			$options['portRangeMin'] = $portRangeMin; 
		}
		if (isset($portRangeMax))
		{
			$options['portRangeMax'] = $portRangeMax; 
		}
		if (isset($protocol))
		{
			$options['protocol'] = $protocol; 
		}
		if (isset($remoteGroupId))
		{
			$options['remoteGroupId'] = $remoteGroupId; 
		}
		if (isset($remoteIpPrefix))
		{
			$options['remoteIpPrefix'] = $remoteIpPrefix; 
		}
		try
		{
			$this->libClass->createSecurityGroupRule($options);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}

	
	/**
	* List of Security Groupes
	*
	* @return void
	*/

	private function listSecurityGroupe()
	{
		try
		{
			$this->app->setOutput("listSecurityGroups", $this->libClass->listSecurityGroups());
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}
	
	
	/**
	* List of Security Groupe Rules
	*
	* @return void
	*/

	private function listSecurityGroupeRule()
	{
		try
		{
			
			$this->app->setOutput("listSecurityGroupeRule", $this->libClass->listSecurityGroupRules());

		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}
	
	/**
	* retrieve a specific Security Groupe given
	*
	* @param securityGroupeId ID of security Groupe which we want to get
	*
	* @return void
	*/

	private function getSecurityGroupe()
	{
		try
		{
			$securityGroupId = $this->app->getPostParam("securityGroupeId");
			$securityGroupe = $this->libClass->getSecurityGroupe($securityGroupId);
			$this->app->setOutput("securityGroupe", $securityGroupe);
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
		
	}
	
	/**
	* internal function
	*
	* retrieve a specific Security Groupe given
	*
	* @param securityGroupeId ID of security Groupe which we want to get
	*
	* @return securityGroup
	*/
	private function getSecurityGroupeP($securityGroupeId)
	{
		try
		{
			$securityGroupe = $this->libClass->getSecurityGroupe($securityGroupeId);
			return $securityGroupe;
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
		
	}
	/**
	* Delete a specific Security Groupe given
	*
	* @param securityGroupeId ID of security Groupe which we want to get
	*
	* @return void
	*/
	private function deleteSecurityGroupe()
	{	
		try
		{
			$securityGroupId = $this->app->getPostParam("securityGroupeId");
			$securityGroupe = getSecurityGroupeP($securityGroupId);
			$securityGroupe->delete();
		}
		catch(BadResponseError $e)
		{
			$this->app->getErrorInstance->BadResponseHandler($e);
		}
		catch(UserInputError $e)
		{
			$this->app->getErrorInstance->UserInputHandler($e);
		}
		catch(BaseError $e)
		{
			$this->app->getErrorInstance->BaseErrorHandler($e);
		}
		catch(NotImplementedError $e)
		{
			$this->app->getErrorInstance->NotImplementedHandler($e);
		}
	}
}	
