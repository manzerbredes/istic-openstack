<?php
/**
* File containing the errorManagement Class.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author  Eole 'eoledev at outlook . fr', Evan Pisani 'yogg at epsina . com'
* 
*/

use OpenCloud\Common\Error\BadResponseError;
use OpenCloud\Common\Error\BaseError;
use OpenCloud\Common\Error\NotImplementedError;
use OpenCloud\Common\Error\UserInputError;

/**
* errorManagement Class of the back-end application
*
* Management of error
*
*/
Class errorManagement{
	
	/** @var App $app protected, contains the main app object */
	protected $app;
	
	/**
	* ErrorManagemement constructor
	*
	* @param App $args the main app object
	*
	* @return ErrorManagement Object
	*/
	public function __construct($args){

		$this->app = $args;

	}
	
	/**
	* Put an error message corresponding to a base error in the output
	*
	* @param Exception $error the exception triggered
	*
	* @return void
	*/
	public function BaseErrorHandler($error){
		$this->app->setOutput("Error", "BaseError");
	}
	
	/**
	* Put an error message corresponding to a bad response in function of the status code in the output
	*
	* @param Exception $error the exception triggered
	*
	* @return void
	*/
	public function BadResponseHandler($error){
		$statusCode = $error->getResponse()->getStatusCode();
		switch ($statusCode) {
		case 400:
			$this->app->setOutput("Error", "Invalid input.");
			break;

		case 401:
			$this->app->setOutput("Error", "Authentification failed.");
			break;

		case 403:
			$this->app->setOutput("Error", "Operation forbidden.");
			break;

		case 404:
			$this->app->setOutput("Error", "Ressource not found.");
			break;

		case 500:
			$this->app->setOutput("Error", "Internal server error, please contact an administrator.");
			break;

		case 503:
			$this->app->setOutput("Error", "Service unvailable for the moment.");
			break;
			
		default:
			$this->app->setOutput("Error", "Unknow error, please contact an administrator.");
			break;
		}	
	}
	
	/**
	* Put an error message corresponding to a not implemented yet error in the output
	*
	* @param Exception $error the exception triggered
	*
	* @return void
	*/
	public function NotImplementedHandler($error){
		$this->app->setOutput("Error", "Internal error (not implemented yet), please contact an administrator");
	}
	
	/**
	* Put an error message corresponding to a user input error in the output
	*
	* @param Exception $error the exception triggered
	*
	* @return void
	*/
	public function UserInputHandler($error){
		$this->app->setOutput("Error", "UserInputError");
	}

	/**
	* Put an error message corresponding to an other error in the output
	*
	* @param Exception $error the exception triggered
	*
	* @return void
	*/
	public function OtherException($error){
		$this->app->setOutput("Error", $error->getMessage());
	}
	
}

?>
