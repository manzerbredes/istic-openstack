<?php
/**
* File containing the Automating Class.
*
* @version 1.0 Initialisation of this file
* @since 1.0 Core application's file
* 
* @author Evan Pisani 'yogg at epsina . com', bhupi
* 
*/

require_once("Image.php");
require_once("Network.php");
require_once("Compute.php");
require_once("NetworkLayer3.php");
require_once("CoreInterface.php");

/**
* automating Class of the back-end application
*
* Contains the different function to automate some action
*
*/
class automating implements Core{

	/** @var App $compute protected, contains a Core compute object */
	protected $compute;
	/** @var App $image protected, contains a Core image object */
	protected $image;
	/** @var App $network protected, contains a Core network object */
	protected $network;
	/** @var App $networkLayer3 protected, contains a Core networkLayer3 object */
	protected $networkLayer3;
	/** @var App $app protected, contains the main app object */
	protected $app;
	
	/**
	* automating class constructor
	*
	* @param App $app the main app object
	*
	* @return automating Object
	*/
	public function __construct($app){
		$this->app = $app;
		$compute = new Compute($app);
		$image = new Image($app);
		$network = new Network($app);
		$networkLayer3 = new NetworkLayer3($app);
	}

	/**
	* Execute an action
	*
	* @param String $action name of another function of this class
	*
	* @return void
	*/
	public function action($action){
		$this->{$action.""}();	
	}
	
	/**
	* create a new server and associate a public ip
	*
	* @param String $networkId the id of the network where the server will be created
	* @param String $imageName name of the new image
	* @param String $serverName name ofthe new server
	* @param String $flavor kind of server
	*	
	* @return void
	*/
	private function createPublicServer()
	{
		$networkId = $this->app->getPostParam('networkId');
		$imageName = $this->app->getPostParam('imageName');
		$serverName = $this->app->getPostParam('serverName');
		$flavor = $this->app->getPostParam('flavor');

		if(!isset($imageName)){
			$this->app->setOutput("Error", "Incorrect imageName parameter"); 
		}
		else if(!isset($serverName)){
			$this->app->setOutput("Error", "Incorrect serverName parameter");
		}
		else if(!isset($flavor)){
			$this->app->setOutput("Error", "Incorrect flavor parameter");
		}
		else{
			// Création image
			$opt = array();
			$opt['name'] = $imageName;
			$image->setPostParam('opt', $opt);	
			$image->action("createImage");
			$image = json_decode($this->app->show(), true)["Images"];

			// Création server
			$compute->setPostParam('name', $serverName);
			$compute->setPostParam('imageId', $image['id']);
			$compute->setPostParam('flavorId', $flavor);
			$compute->action("createServer");
			$server = json_decode($this->app->show(), true)["Compute"];

			// liste des adresses ip publiques diponibles
			$networkLayer3->action("listFloatingIp"); 
			$listFloatingIp =  json_decode($App->show(), true)["NetworkLayer3"];
			$ip = null;
			foreach ($listFloatingIp as $f) {
				if(strcmp($f['status'], "DOWN")){
					$ip = $f;
				}
			}

			// Si pas d'ip publique disponible on en créé une
			if(!isset($ip)){ 
				// Ajout adresse IP public
				$optIp = array();
				$opt['floatingNetworkId'] = $networkId;
				$floatingIp->setPostParam('opt', $optIp);	
				$networkLayer3->action("createFloatingIp");
				$ip = json_decode($App->show(), true)["NetworkLayer3"];
			}

			// Association de l'ip publique au serveur	
			/*
			* API non diponible pour le moment
			*/  

		}
	}
}

?>
