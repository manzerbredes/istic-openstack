
/**
 * The main app module instance
 * @type angular.module.angular-1_3_6_L1749.moduleInstance
 */
var mainApp=angular.module("mainApp",['ngRoute', 'ngSanitize', 'ngCookies','lr.upload']);

/**
 * Configure routeProvider
 */
mainApp.config(['$routeProvider', function($routeProvider){
        
        
        $routeProvider.
            when('/home',{
                templateUrl: 'partials/home/home.html'
                //controller: 'homeCtrl'
            }).
            when('/network',{
                templateUrl: 'partials/network/network.html'
                //controller: 'networkCtrl'
            }).
            when('/image',{
                templateUrl: 'partials/image/image.html'
                //controller: 'imageCtrl'
            })
            .otherwise({
                redirectTo: '/home'
        });
}]);

/**
 * Configure httpProvider
 */
mainApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);

