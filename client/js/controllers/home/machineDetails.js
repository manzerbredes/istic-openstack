/**
 * The home controller
 * 
 * @param {$scope} $scope The $scope service from angular
 */
mainApp.controller('machineDetailsCtrl', ['$scope', 'Compute', '$rootScope', '$timeout', 'Identity', 'Image', function ($scope, Compute, $rootScope, $timeout, Identity, Image)
    {


// Init scope
        $scope.machine = {};
        $scope.machineIsStarting = false; // For loading icon

        // When we need to show details of machine
        $scope.$on('showMachineDetailsEvent', function (eventName, machine, axioms) {
            $scope.machine = machine;
            // console.log(machine.flavor)
            $scope.axioms = axioms;
            Image.getImages(function(response){
                $scope.images = Image.getData().images;
            });
            $('#machineDetailsModal').modal({backdrop: false, keyboard: true});
        });
        // Try to stop or start a machine
        $scope.toggleMachineState = function () {
            // Display gif
            $scope.machineIsStarting = true;
            // Fake timeout
            $timeout(function () {
                $scope.machineIsStarting = false;
            }, 3000);
            $timeout(function () {
                $scope.machine.online = !$scope.machine.online;
            }, 3000);
        };
        // Apply modifications
        $scope.applyModifications = function () {
            //Todo
        };
        $scope.deleteMachine = function () {
            var call = function () {
                Compute.pullData(function () {
                    $rootScope.$broadcast("updateGraphEvent");
                });
            }



            Compute.deleteMachine(call, $scope.machine.id);
        }

    }]);
