/**
 * The home controller
 * 
 * @param {$scope} $scope The $scope service from angular
 */
mainApp.controller('machineCreationCtrl', ['$scope', 'Compute', '$rootScope', '$timeout', 'Identity', 'Image', function ($scope, Compute, $rootScope, $timeout, Identity, Image)
    {

        $scope.name = "";
        $('#pleaseChooseAnImage').hide();
        // When we need to show details of machine
        $scope.$on('showMachineCreationEvent', function (eventName, axioms) {
            $scope.axioms = axioms;
            Image.getImages(function(response){
                $scope.images = Image.getData().images;
            });
            $('#machineCreationModal').modal({backdrop: false, keyboard: true});
        });

        var callMeAfterMachineCreation=function(response){
            Compute.pullData(function(){$rootScope.$broadcast("updateGraphEvent")});
        };

        $scope.createMachine = function () {
            if ($scope.selectedImage == null) {
                $('#pleaseChooseAnImage').show();
            } else {
                $('#pleaseChooseAnImage').hide();
                $('#machineCreationModal').modal("hide");

                machine = {}
                machine.name = $scope.name
                machine.flavorId = 1
                machine.imageId = $scope.selectedImage

                
                Compute.createMachine(callMeAfterMachineCreation, machine)
                $scope.name="";

            }
        };

    }]);
