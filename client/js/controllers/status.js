

/**
 * The status controller
 * 
 * @param {$scope} $scope The $scope service from angular
 * @param {Identity} The Identity service
 */
mainApp.controller('statusCtrl', ['$scope', 'Identity', '$rootScope', function ($scope, Identity, $rootScope)
    {
        // Give profile to model
        $scope.profile = Identity.getProfile();

        // Function to logout
        $scope.logout = function () {
            Identity.logout();
        };

    }]);
