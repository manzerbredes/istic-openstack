/**
 * The image controller
 * 
 * @param {$scope} $scope The $scope service from angular
 */
mainApp.controller('createImageCtrl', ['$scope', 'Image', 'Loading', 'Identity', 'upload','$rootScope', function ($scope, Image, Loading, Identity, upload, $rootScope)
    {
        /*$scope.uploader = new FileUploader({
         "token" : Identity.getToken(),
         "task" : "image",
         'action':'uploadImage',
         'id':'6564'
         });
         $scope.uploader.url='../server/index.php'
         $scope.uploader.alias='file_name'
         $scope.uploader.formData={
         "token" : Identity.getToken(),
         "task" : "image",
         'action':'uploadImage',
         'id':'6564'
         }
         */

        $scope.doUpload = function () {
            /* console.log($('#imageToUpload').prop('files')[0]);
             Image.uploadImage($('#imageToUpload').prop('files')[0], function () {
             alert("done");
             });*/
            Image.uploadImage("loic", function () {})
            /*$("#drop-area-div").dmUploader({
             extraData: 	{
             "token" : Identity.getToken(),
             "task" : "image",
             'action':'uploadImage',
             'id':'6564'},
             url:"../server/index.php"
             });
             */

            /*upload({
             url: '../server/index.php',
             method: 'POST',
             data: {
             "token" : Identity.getToken(),
             "task" : "image",
             'action':'uploadImage',
             'id':'6564',
             "file_name": $scope.myFile, // a jqLite type="file" element, upload() will extract all the files from the input and put them into the FormData object before sending.
             }
             }).then(
             function (response) {
             console.log(response.data); // will output whatever you choose to return from the server on a successful upload
             },
             function (response) {
             console.error(response); //  Will return if status code is above 200 and lower than 300, same as $http
             }
             );*/


        };
        // Manager logout event
        $scope.$on('showCreateImageModalEvent', function () {
            $scope.token = Identity.getToken();

            $('#createImageModal').modal("show");
        });

        var callMeAfterImageCreate = function (response) {
            $rootScope.$broadcast("updateImageEvent");
            Loading.stop();
            
        }

        $scope.doCreation = function () {
            $('#createImageModal').modal("hide");
            Loading.start();
            Image.createImage($scope.name, callMeAfterImageCreate)
        };

    }]);
