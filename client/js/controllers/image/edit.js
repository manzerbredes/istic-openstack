/**
 * The image controller
 * 
 * @param {$scope} $scope The $scope service from angular
 */
mainApp.controller('editImageCtrl', ['$scope', 'Image', 'Loading', 'Identity', 'upload', '$rootScope', function ($scope, Image, Loading, Identity, upload, $rootScope)
    {
        $scope.$on('editImageEvent', function (eventName, image, axioms) {
						var list = $("#upload_button");
						$.each(list, function(){$(this).detach();});
            $scope.image = image;
            $scope.data = {};
            $scope.data.id = image.id;
            if (image.protected) {
                $scope.data.protected = "true";

            } else {
                $scope.data.protected = "false";
            }
            $scope.data.name = image.name;
            $scope.data.visibility = image.visibility;

            $scope.axioms = axioms;
						$('#progress .bar').css(
										'width',
										'0%'
								);
						if(image.status != "queued"){
							$("#fileupload").css('display','none');
							$("#fileupload").val('');
							$("#progress").css('display', 'none');
						}else{
							$("#fileupload").val('');
							$("#fileupload").css('display','block');
							$("#progress").css('display', 'block');
							$("#fileupload").fileupload({
								replaceFileInput: false,
								formData: {task: "image", token: Identity.getToken(), action: "uploadImage", id: $scope.data.id},
							/* ... */
								progressall: function (e, data) {
									var progress = parseInt(data.loaded / data.total * 100, 10);
									$('#progress_text').html(progress+'%');
									$('#progress .bar').css(
											'width',
											progress + '%'
									);
								},
								add: function (e, data) {
										data.process();
										var list = $("#upload_button");
										$.each(list, function(){$(this).detach();});
										data.context = $('<button id="upload_button"/>').text('Upload')
												.click(function () {
														$(this).detach();
														data.submit();
												});
										console.log("test");
										$("#fileupload").after(data.context);
								},
							});
						}
            $('#editImageModal').modal('show');
						
        });

        $scope.applyEdition = function (id) {
            Image.updateImage($scope.data, function () {
                $rootScope.$broadcast("updateImageEvent");
                $('#editImageModal').modal('hide');

            });

        };

        $scope.getToken = function () {
            return Identity.getToken();
        }
			
				
    }]);
