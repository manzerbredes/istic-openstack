/**
 * The image controller
 * 
 * @param {$scope} $scope The $scope service from angular
 */
mainApp.controller('imageCtrl', ['$scope', 'Image', 'Loading', 'Identity', '$rootScope', function ($scope, Image, Loading, Identity, $rootScope)
    {

        // Update view
        var callMeAfterGetImage = function () {
            $scope.images = Image.getData().images;
            Loading.stop();
        };

        // If user is login try to retrieve data
        if (Identity.isAlreadyLogin()) {
            Loading.start();
            Image.getImages(callMeAfterGetImage);

        }

        // Manager logout event
        $scope.$on('updateImageEvent', function () {
            // If user is login try to retrieve data
            if (Identity.isAlreadyLogin()) {
                Loading.start();
                Image.getImages(callMeAfterGetImage);
            }
        });

        $scope.edit = function (image) {
            $rootScope.$broadcast("editImageEvent", image, Image.getData().axioms);

        }

        $scope.showCreateImageModal = function () {
            $rootScope.$broadcast("showCreateImageModalEvent");

        };

        $scope.deleteImage = function (id) {
            Image.deleteImage(id, function () {
                $rootScope.$broadcast("updateImageEvent");

            })
        }
    }]);
