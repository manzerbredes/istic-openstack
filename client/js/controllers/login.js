
/**
 * The login controler
 * @param {$scope} $scope The $scope angular service
 * @param {$sce} $sce The $sce angular service
 * @param {$http} $http The $http angular service
 * @param {Identity} The Identity service
 
 */
mainApp.controller('loginCtrl', ['$scope', '$sce', 'Identity', '$rootScope', function ($scope, $sce, Identity, $rootScope)
    {
        // Check for login and define default states
        if (!Identity.isAlreadyLogin()) {
            $('#loginModal').modal({backdrop: 'static', keyboard: false});
        }

        // Manager logout event
        $scope.$on('logoutEvent', function () {
            $('#loginModal').modal({backdrop: 'static', keyboard: false});
        });



        // Hide loading button and message alert
        $('#loadingLoginButton').hide();
        $('#failedToLoginAlert').hide();


        // Defined function for login
        $scope.loginAction = function () {

            // Begin login state for template
            $('#loginButton').hide();
            $('#loadingLoginButton').show();
            $('#failedToLoginAlert').hide();

            // Get data from templates
            var username = $("#loginFormUsername").val();
            var password = $("#loginFormPassword").val();
            var projectname = $("#loginFormProjectname").val();

            // Function to call to handle result
            var responseCallback = function (response) {

                if (response.status !== 0) {
                    // Set reason of fail
                    $scope.failReason = response.failReason;

                    // Display the error
                    $('#failedToLoginAlert').show();
                } else {
                    // Else the user is online !
                    $('#loginModal').modal('hide');
                    // Send login event
                    $rootScope.$broadcast("loginEvent");
                }

                // Reset button state
                $('#loginButton').show();
                $('#loadingLoginButton').hide();
            };

            // Try to login
            Identity.login(username, password, projectname, responseCallback);
        };

    }]);
