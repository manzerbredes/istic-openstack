
mainApp.factory('Compute', ['$http', 'Identity', function ($http, Identity) {


        // Init data
        var data = {};
        data.machines = null;
        data.axioms = {} // Contain static data
        data.axioms.ram = [128, 512, 1024, 2048, 4096];
        data.axioms.disk = [1, 2, 5, 10, 25, 50, 100, 150, 200]
        data.axioms.images = {}; // Retrieve after


        /**
         * Parse pullMachines answer
         * @param {response} the server response
         * @param {boolean} false if the request as been send true else
         * @return {requestParserResult} the result of parsing
         */
        var parsePullMachinesAnswer = function (response, failedToSendRequest) {

            // Defined return object
            var requestParserResult = {};
            requestParserResult.status = 1;
            requestParserResult.failReason = null;


            if (typeof response.data.Servers !== 'undefined') {
                // Set status code
                requestParserResult.status = 0;
                data.machines = response.data.Servers;

            } else if (failedToSendRequest) {
                requestParserResult.failReason = "Failed to send PullMachine request";
            } else {
                requestParserResult.failReason = "Error";
            }
            return requestParserResult;
        };


        /**
         * Retrieve machine list
         * @param {type} callback function to call after request complete
         * @returns {undefined}
         */
        var pullMachines = function (callback) {
            // Send listServers request
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "compute", "action": "listServers"}));

            // Wait and handle the response
            result.then(function (response) {
                callback(parsePullMachinesAnswer(response, false));
            }, function (response) {
                callback(parsePullMachinesAnswer(response, true));
            });
        };


        /**
         * Parse pullImages answer
         * @param {response} the server response
         * @param {boolean} false if the request as been send true else
         * @return {requestParserResult} the result of parsing
         */
        var parsePullImagesAnswer = function (response, failedToSendRequest) {

            // Defined return object
            var requestParserResult = {};
            requestParserResult.status = 1;
            requestParserResult.failReason = null;


            if (typeof response.data.Images !== 'undefined') {
                // Set status code
                requestParserResult.status = 0;
                data.axioms.images = response.data.Images;
            } else if (failedToSendRequest) {
                requestParserResult.failReason = "Failed to send PullImage request";
            } else {
                requestParserResult.failReason = "Error";
            }
            return requestParserResult;
        };




        /**
         * Retrieve machine list 
         * @param {callback} function to call after request complete
         */
        var pullImages = function (callback) {
            // Send listServers request
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "compute", "action": "listImages"}));

            // Wait and handle the response
            result.then(function (response) {
                callback(parsePullImagesAnswer(response, false));
            }, function (response) {

                callback(parsePullImagesAnswer(response, true));
            });
        };

        var createMachine = function (callback, machine) {
            // Send listServers request
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "compute", "action": "createServer", 'name': machine.name, "imageId": machine.imageId, "flavorId": machine.flavorId}));

            // Wait and handle the response
            result.then(function (response) {
                console.log(response.data)
                                callback();

            }, function (response) {
                console.log("error")
                callback();
            });
        };

        var deleteMachine = function (callback, machineId) {
            // Send listServers request
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "compute", "action": "deleteServer", 'serverId': machineId}));

            // Wait and handle the response
            result.then(function (response) {
                
                callback();
                console.log(response.data.Error)
            }, function (response) {
                console.log("error")
                callback();
            });
        };


        /**
         * Retrieve all data 
         * @param {callback} function to call after request complete
         */
        var pullData = function (callback) {
            var nextFunction = function (response) {
                if (response.status == 0) {
                    pullMachines(callback);
                } else {
                    callback(response);
                }
            };
            pullImages(nextFunction);
        };


        /**
         * Get Data
         * @return {data} return the data object
         */
        var getData = function () {
            return data;
        };

        // Return services objects
        return {
            pullMachines: pullMachines,
            pullData: pullData,
            createMachine: createMachine,
            deleteMachine: deleteMachine,
            getData: getData
        };


    }]);
