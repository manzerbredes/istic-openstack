
mainApp.factory('Image', ['$http', 'Identity', function ($http, Identity) {

        // Data object
        var data = {};
        data.images = null; // Images
        data.axioms = {};
        data.axioms.protected = ["false", "true"];
        data.axioms.visibility = ["public", "private"];
        /**
         *  Parse uploadImage anwser
         * @param {type} response
         * @param {type} failedToSendRequest
         * @returns {Image_L2.parseUploadImageAnswer.requestParserResult}
         */
        var parseUploadImageAnswer = function (response, failedToSendRequest) {

            // Defined return object
            var requestParserResult = {};
            requestParserResult.status = 1;
            requestParserResult.failReason = null;


            if (typeof response.data.Images !== 'undefined') {
                // Set status code
                requestParserResult.status = 0;
                data.images = response.data.Images;


            } else if (failedToSendRequest) {
                requestParserResult.failReason = "Failed to send request";
            } else {
                requestParserResult.failReason = "Error";
            }
            return requestParserResult;
        };


        /**
         *  Get images
         * @param {type} callback
         * @returns {undefined}
         */
        var getImages = function (callback) {

            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "image", 'action': 'listImage'}));

            // Wait and handle the response
            result.then(function (response) {
                callback(parseUploadImageAnswer(response, false));
            }, function (response) {
                callback(parseUploadImageAnswer(response, true));
            });


        };

        /**
         * Update image
         * @param {type} image
         * @param {type} callback
         * @returns {undefined}
         */
        var updateImage = function (image, callback) {
            console.log(image)
    
        
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "image", 'action': 'updateImage', 'id': image.id, 'opt': {'name': image.name, 'visibility':image.visibility, 'protected':image.protected}}));

            // Wait and handle the response
            result.then(function (response) {
                console.log(response.data.Images)
                callback();
            }, function (response) {
                alert(response);
            });


        };



        /**
         * Upload an image
         * @param {type} fileToUpload
         * @param {type} callback
         * @returns {undefined}
         */
        var uploadImage = function (fileToUpload, callback) {
            /*var form_data = new FormData();
             form_data.append('file', fileToUpload);
             console.log(fileToUpload);
             form_data.append("task", "image");
             form_data.append("token", Identity.getToken());
             form_data.append('action', "uploadImage");
             form_data.append('id', '6564');
             form_data.append('file_name', fileToUpload);
             
             $.ajax({
             url: "../server/index.php", // Url to which the request is send
             type: "POST", // Type of request to be send, called as method
             data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
             file_name: fileToUpload,
             token: Identity.getToken(),
             task: "image",
             action: 'uploadImage',
             id: '6564',
             contentType: false, // The content type used when sending data to the server.
             cache: false, // To unable request pages to be cached
             processData: false, // To send DOMDocument or non processed data file it is set to false
             success: function (data)   // A function to be called if request succeeds
             {
             alert("success");
             }
             });*/

            alert("send")
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "image", 'action': 'createImage', "opt": {"name": "loic"}}));

            //var result=$http.post('../server/index.php',
            //				  $.param({"token" : Identity.getToken(), "task" : "image", 'action':'uploadImage', 'file_name':form_data, 'id':'6564'}));  

            // Wait and handle the response
            result.then(function (response) {
                //callback(parseUploadImageAnswer(response, false));
                alert(response.data)
                console.log(response.data)
                callback()
            }, function (response) {
                alert(response.data)
                callback()
                //callback(parseUploadImageAnswer(response, true));
            });
        };

        var createImage = function (name, callback) {


            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "image", 'action': 'createImage', "opt": {"name": name}}));

            //var result=$http.post('../server/index.php',
            //				  $.param({"token" : Identity.getToken(), "task" : "image", 'action':'uploadImage', 'file_name':form_data, 'id':'6564'}));  

            // Wait and handle the response
            result.then(function (response) {
                callback()
            }, function (response) {
                alert(response.data)
                callback()
            });
        };
        var deleteImage = function (id, callback) {


            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "image", 'action': 'deleteImage', "id": id}));

            //var result=$http.post('../server/index.php',
            //				  $.param({"token" : Identity.getToken(), "task" : "image", 'action':'uploadImage', 'file_name':form_data, 'id':'6564'}));  

            // Wait and handle the response
            result.then(function (response) {
                callback()
            }, function (response) {
                alert(response.data)
                callback()
            });
        };



        var getData = function (response) {
            return data;
        };

        // Return services objects
        return {
            getImages: getImages,
            updateImage: updateImage,
            getData: getData,
            deleteImage: deleteImage,
            createImage: createImage,
            uploadImage: uploadImage
        };


    }]);
