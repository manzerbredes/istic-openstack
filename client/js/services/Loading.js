/**
 * Loading service
 * @param {type} param1
 * @param {type} param2
 */
mainApp.factory('Loading', [function () {
        /**
         * Display Loading modal
         */
        var start = function () {
            $('#loadingModal').modal({backdrop: 'static', keyboard: false});
        };

        /**
         * Hide Loading modal
         */
        var stop = function () {
            $('#loadingModal').modal('hide');
        };

        // Service returns
        return {
            start: start,
            stop: stop
        };
    }]);
