
mainApp.factory('Network', ['$http', 'Identity', function ($http, Identity) {

        // Data object
        var data = {};
        data.networks = null; // Networks

        /**
         * ListId
         * @param {type} fileToUpload
         * @param {type} callback
         * @returns {undefined}
         */
        var ListId = function (fileToUpload, callback) {
            var result = $http.post('../server/index.php',
                    $.param({"token": Identity.getToken(), "task": "network", 'action': 'list_network_ids'}));

            // Wait and handle the response
            result.then(function (response) {
                callback(parseUploadImageAnswer(response, false));
            }, function (response) {
                callback(parseUploadImageAnswer(response, true));
            });
        };


    }]);


